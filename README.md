# FM / AM demodulator project status description

At the moment, the project includes:
* _2 files with HBF  and final LPF at 20kHz coefficients_
* _Script of the function for obtaining LPF coefficients before demodulation_
* _Load script for initial simulation conditions_
* _Simulink. Model completely in fixedpoint except for LPF filter coefficients_
